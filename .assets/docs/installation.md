# Installation

> **⚠️ Use this configuration at your own risk! ⚠️** 

- [Easy Installation (Gnome ISO + install.sh)](#easy-installation-gnome-iso)
- [Manual Installation (minimal ISO) - *outdated*](#manual-installation-minimal-iso)

## Easy Installation (Gnome ISO)

1. **Install NixOS**

   Install nixos using any [graphical ISO image](https://nixos.org/download.html#nixos-iso) and use the `no desktop` option

2. **Clone the repo**

```shell
# enable git
nix-shell -p git

cd ~
git clone https://gitlab.com/d0mu/nixos-config
cd nixos-config
```

3. **Modify or remove Git Config**

   Change the git account yourself in `./modules/git/default.nix`

```
   programs.git = {
      ...
      userName = "git-user";
      userEmail = "git-email";
      ...
   };
```

4. **Execute Install script**
   
   Do NOT execute it as root!

```shell
./install.sh
```

5. **Reboot**

   After rebooting, you'll be greeted by swaylock


### Post-Install

**Git SSH Key**

```shell
# Create the key
ssh-keygen -t ed25519 -C "<yours>@users.noreply.gitlab.com"

# By default, key will be saved to
/home/<user>/.ssh/id_ed25519.pub
```

Add the key to GitLab

----

**VMWare Workstation**

> Currently not used in favour of `virt-manager` (qemu)

If you wish to use vmware workstation you will need to add your serial key.

You can do so with the following command:

```shell
sudo vmware-vmx --new-sn XXXXX-XXXXX-XXXXX-XXXXX-XXXXX
```

## Manual Installation (minimal ISO)

*outdated*

Official Guide: https://nixos.org/manual/nixos/stable/#sec-installation-manual

### Prep

Download the minimal NixOS [ISO](https://nixos.org/download)

```bash
wget -O https://channels.nixos.org/nixos-23.11/latest-nixos-minimal-x86_64-linux.iso
```

Create and Boot into the installer with USB (booting with UEFI)

Switch to Root: `sudo -i`

### Disk Setup

#### Create GPT(UEFI) Partitions

*Check official installation manual linked above if you need Legacy Boot*

```shell
# see the devices
fdisk -l
lsblk

# Create GPT partition table on the drive
parted /dev/sda -- mklabel gpt

# Create the ROOT partition (1)
## Leaving 1GB at start for boot, and 8GB at the end for swap
parted /dev/sda -- mkpart root ext4 1GB -8GB

# Create SWAP partition (2)
parted /dev/sda -- mkpart swap linux-swap -8GB 100%

# Create the BOOT partition (3)
parted /dev/sda -- mkpart ESP fat32 1MB 1GB
parted /dev/sda -- set 3 esp on

```

#### Format Partitions

```shell
# main, ext4, nixos as the label
mkfs.ext4 -L nixos /dev/sda1

# swap
mkswap -L swap /dev/sda2

# boot
mkfs.fat -F 32 -n boot /dev/sda3

# Check
fdisk -l
lsblk
```

#### Mount Partitions

```shell
# mount the main partition (1)
mount /dev/disk/by-label/nixos /mnt

# mount boot (UEFI) (3)
mkdir -p /mnt/boot
mount /dev/disk/by-label/boot /mnt/boot

# activate swap (2)
swapon /dev/sda2
```

> **Note about Boot Config for UEFI**
> (*from NixOS installation wiki*)
> You must select a boot-loader, either systemd-boot or GRUB. 
> 
> The recommended option is **systemd-boot**: set the option `boot.loader.systemd-boot.enable` to *true*. nixos-generate-config should do this automatically for new configurations when booted in UEFI mode. (optional) You may want to look at the options starting with boot.loader.efi and boot.loader.systemd-boot as well .
>
> If you want to use **GRUB**, set `boot.loader.grub.device` to *nodev* and `boot.loader.grub.efiSupport` to *true*.
> 
> With systemd-boot, you should not need any special configuration to detect other installed systems. With GRUB, set `boot.loader.grub.useOSProber` to true, but this will only detect windows partitions, not other Linux distributions. If you dual boot another Linux distribution, use systemd-boot instead.



### Enable Flakes & Download Config

Enable nixFlakes

```bash
nix-shell -p git nixFlakes nixUnstable
```

Clone NixOS Configs

```bash 
git clone https://gitlab.com/d0mu/nixos-configs /mnt/etc/nixos
```

### Create Hardware Config & Place it

Generate hardware configuration

```shell
nixos-generate-config --root /mnt
```

Move it to your hosts folder

```shell
cd /mnt/etc/nixos

cp hardware-configuration hosts/<your host>/

# delete the configuration.nix it created
rm configuration.nix
```

### Install the Flake

```bash
# Set temp dir on /mnt so we don't run into any issues
## see: https://stackoverflow.com/questions/76591674/nix-gives-no-space-left-on-device-even-though-nix-has-lots
mkdir -p /mnt/temp
export TMPDIR=/mnt/temp

# Install
nixos-install --flake .#nixpc
```

### Post-Install Steps

Reboot

login as root, and change the password for your user using `passwd username` *(you may need to change tty)*

`logout` and log in as your normal user


### Install Home Manager Config

```bash
sudo chown -R $USER /etc/nixos # change ownership of configuration folder
cd /etc/nixos

# Create the profiles folder as it may not be created by nix installer
## see: https://old.reddit.com/r/Nix/comments/1443k3o/home_manager_installation_could_not_find_suitable/
mkdir -p ~/.local/state/nix/profiles

home-manager switch --flake '.#domu'
```

Reboot one last time
