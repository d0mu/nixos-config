# NixOS

> NixOS revolutionises the way we manage Linux systems. It uses the Nix package manager to provide atomic, reproducible, and isolated package installations. The entire system configuration, from packages to services and users, is declared in configuration files. This declarative approach allows for easy version control, rollbacks, and collaborative system management. NixOS ensures consistency across environments, making it ideal for both personal use and large-scale deployments where reliability, scalability, and reproducibility are paramount.

# My Nix Config Structure

- `flake.nix` : Base of the configuration
- /hosts : Per-host configurations that contain machine specific configurations
  - /nixos : Default host
- /modules : Modular NixOS configurations
  - /core : Core NixOS configuration
  - /home : Home-Manager config
- /pkgs : Custom packages exported by the flake (currently none)
- /wallpapers : Wallpapers collection


## Update Configs & Rebuild Flake

Nix Flake + Home-Manager


```shell
# Nix Flake + Home
sudo nixos-rebuild switch --flake ~/nixos-config#nixos
#sudo nixos-rebuild switch  # standard if configs at /etc/nixos

# (For Home-Manager default, installed as a module)
## When running sudo nixos-rebuild switch, the configuration of home-manager will be applied automatically. 

# Home-Manager Standalone (anything in /home)
#home-manager switch --flake '.#domu'
```

## Update System

```shell
# Update flake.lock
sudo nix flake update ~/nixos-config#
#sudo nix flake update  # standard if configs at /etc/nixos

# Apply the updates, you need to specify the flake
sudo nixos-rebuild switch --flake ~/nixos-config#nixos
#sudo nixos-rebuild switch --flake .#nixos  # standard if configs at /etc/nixos
```

# Other tidbits

## Flatpaks & AppImages

- `flatpak` is enabled for `bottles` (windows apps, e.g: wow)
  - need to add the flathub repo after installation, and install bottles manually:
  - `flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo`
  - `flatpak install bottles`
- `appimage-run` is enabled for `wowup-cf` (wow addons)
  - download the AppImage and run it: `appimage-run $AppImageFile`

### Bottles

- install the latest `wine-ge` runner in bottles settings
- use that for the wow bottle and install bnet using the built-in script within the bottle


## Rebuild Boot

Useful after garbage cleanup, it'll remove old grub entries if that job didn't run

```shell
sudo nixos-rebuild boot --flake ~/nixos-config#nixos 
```

*Note: this has been added to the garbage-cleanup alias!*

## Nvidia

Both of the hosts currently setup in this repo use nvidia graphics

- `/modules/home/hyprland` contains nvidia configuration (in `variables.nix`)
- `/modules/core/default.nix` sources `nvidia.nix`

> See: [Nvidia PRIME](https://nixos.wiki/wiki/Nvidia#Laptop_Configuration:_Hybrid_Graphics_.28Nvidia_Optimus_PRIME.29) nixos notes if you use have a laptop with hybrid graphics. Currently I haven't had to use any of this configuration.

## Swap

After luks, if for some reason your boot is hanging on "waiting for device <uuid>" then it may be your swap partition if you chose to have one

```shell
# check if its marked as swap
sudo fdisk -l

# make it swap
sudo mkswap -L swap /dev/nvme1n1p3

# activate swap
sudo swapon /dev/nvme1n1p3
```

## Testing in a VM

If you would like to test this in a vm, the following files need updating:

- `/modules/home/hyprland/variables.nix`
```shell
# Vmware-----------------------------------
env = WLR_NO_HARDWARE_CURSORS=1
env = WLR_RENDERER_ALLOW_SOFTWARE=1
```

If using vmware for the vm you also need to edit:

- `/modules/core/virtualisation.nix`
```shell
# Vmware guest additions
virtualisation.vmware.guest.enable = true;