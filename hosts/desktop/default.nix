{ pkgs, config, ... }: 
{
  imports = [
    ./hardware-configuration.nix
    ./../../modules/core
  ];

  #networking.hostName = "nixpc";

  powerManagement.cpuFreqGovernor = "performance";


  boot = {
    ## default LTS kernel
    kernelPackages = pkgs.linuxPackages;
    ## latest kernel - as of last test, not working with latest nvidia drivers
    ### See: https://discourse.nixos.org/t/nvidia-550-78-linux-6-9-1-breaks-wayland/45717/3
    #kernelPackages = pkgs.linuxPackages_latest;

    # for Netgear A7000 USB WiFi Adapter
    extraModulePackages = with config.boot.kernelPackages; [
        rtl88xxau-aircrack
      ];
  };
}