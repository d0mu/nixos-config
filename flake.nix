{
  description = "Dom's NixOS";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nur.url = "github:nix-community/NUR";
  
    hypr-contrib.url = "github:hyprwm/contrib";
    hyprpicker.url = "github:hyprwm/hyprpicker";
  
    alejandra.url = "github:kamadorueda/alejandra/3.0.0";
  
    #nix-gaming.url = "github:fufexan/nix-gaming";
  
    # Testing cachix hyprland so we don't have to rebuild each time
    hyprland.url = "git+https://github.com/hyprwm/Hyprland?submodules=1";
    #hyprland = {
    #  type = "git";
    #  url = "https://github.com/hyprwm/Hyprland";
    #  submodules = true;
    #};
  
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    
    #spicetify-nix = {
    #  url = "github:gerg-l/spicetify-nix";
    #  inputs.nixpkgs.follows = "nixpkgs";
    #};

    # Grub theme - flake input
    darkmatter-grub-theme = {
      url = gitlab:VandalByte/darkmatter-grub-theme;
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # other themes, to remove one day - Non-Flake inputs
    catppuccin-bat = {
      url = "github:catppuccin/bat";
      flake = false;
    };
    catppuccin-cava = {
      url = "github:catppuccin/cava";
      flake = false;
    };
    #catppuccin-starship = {
    #  url = "github:catppuccin/starship";
    #  flake = false;
    #};
  };

  outputs = { nixpkgs, self, ...} @ inputs:
  let
    username = "domu";
    system = "x86_64-linux";
    pkgs = import nixpkgs {
      inherit system;
      config.allowUnfree = true;
    };
    lib = nixpkgs.lib;
  in
  {
    nixosConfigurations = {
      ### (i) Notes: 
      # flake names are currently used as the hostname
      # flake names are set to nixpc | nixlap | nixvm (mapped to corresponding ./hosts/)
      # hostname is set in modules/core/network.nix based on passed 'host' (hostname) value
      nixpc = nixpkgs.lib.nixosSystem {
        inherit system;
        modules = [ ./hosts/desktop ];
        specialArgs = { host="nixpc"; inherit self inputs username ; };
      };
      nixlap = nixpkgs.lib.nixosSystem {
        inherit system;
        modules = [ ./hosts/laptop ];
        specialArgs = { host="nixlap"; inherit self inputs username ; };
      };
      nixvm = nixpkgs.lib.nixosSystem {
        inherit system;
        modules = [ ./hosts/vm ];
        specialArgs = { host="nixvm"; inherit self inputs username ; };
      };
    };
  };
}
