<div align="center">
    <h1>
        <img src="./.assets/logo/nixos-logo.png  " width="100px" /> 
        <br>
        <img src="https://img.shields.io/badge/NixOS-unstable-blue.svg?style=for-the-badge&labelColor=303446&logo=NixOS&logoColor=white&color=91D7E3">
    </h1>
</div>

- **WM** - hyprland 
- **Shell** - zsh
- **Term** - kitty
- **Application Launcher** - fuzzel / rofi
- **Bar** - waybar
- **Notification Daemon** - swaync
- **Editor** - vscodium / neovim 
- **File Manager** - nautilus / yazi
- **Screenshot** - grimblast
- **Screen Recording** - wf-recorder
- **Clipboard** - wl-clipboard
- **Locking & Idle** - hyprlock / swaylock-effects
- **Browsing** - floorp / firefox (with arkenfox user.js)
- **Image Viewer** - imv
- **Media Player** - mpv
- **Music** - audacious
- **Networking** - networkmanager + applet
- **System Resource Monitor** - btop
- **Fonts** - nerd fonts
- **Colour Scheme** - catppuccin (for now, will have swappable themes soon)
- **Colour Picker** - hyprpicker
- **Cursor** - bibata-modern-ice
- **Icons** - catppuccin papirus

----

> **Notes:**
> - Using Nix Flakes and Home-manager
> - Contains Nvidia configuration, see [nix-notes](.assets/docs/nix-notes.md#nvidia) for details
> - Program overlays can be added in /pkgs

# Docs

- [Installation Guide](.assets/docs/installation.md)
- [Nix Notes](.assets/docs/nix-notes.md)

# Resources

**Nix resources:**
- https://nixos.org/
- https://nixos.wiki/wiki/Main_Page
- https://search.nixos.org/options
- https://search.nixos.org/packages
- https://discourse.nixos.org/t/breaking-changes-announcement-for-unstable/17574/42

**Learning resources:**
- [Intro to NixOS & Flakes](https://thiscute.world/en/posts/nixos-and-flake-basics/)
- [Home-manager as a flake](https://www.chrisportela.com/posts/home-manager-flake/)
- [Nix-starter-configs](https://github.com/Misterio77/nix-starter-config)
- [Nix Options/Packages/Flakes Lookup & Documentation](https://mynixos.com)
- [Zero-to-nix](https://zero-to-nix.com)
- [Pills](https://nixos.org/guides/nix-pills/)

# Credits

With many thanks and full credits to the following repos where I took inspiration, based the config on, and took some scripts.

- https://github.com/Frost-Phoenix/nixos-config (install script + base)
- https://github.com/linuxmobile/kaku/
- https://github.com/johnk1917/nixrland
- https://github.com/chadcat7/crystal
- https://github.com/prasanthrangan/hyprdots
- https://github.com/ryan4yin/nix-config/tree/i3-kickstarter
- https://github.com/ryan4yin/nix-config
