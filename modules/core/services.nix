{ ... }: 
{
  services = {
    gvfs.enable = true;
    gnome.gnome-keyring.enable = true;
    dbus.enable = true;
    fstrim.enable = true;

    blueman.enable = true;

    # Thunar extended functionalities
    #tumbler.enable = true; # Thumbnail support for images

    # Enable Flatpak - Used for Bottles (wine wrapper)
    #flatpak.enable = true;
    # need to run this manually after install:
    # flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

    #printing.enable = true; # Enable CUPS to print documents

    #redshift = {
      #enable = true;
      #brightness = {
        #day = "1";
        #night = "1";
      #};
      #temperature = {
        #day = 5500;
        #night = 3700;
      #};
    #};
  };

  # Location Provider, for RedShift Service
  #location.provider = "geoclue2";

  services.logind.extraConfig = ''
    # don’t shutdown when power button is short-pressed
    HandlePowerKey=ignore
  '';
}
