{ pkgs, host, ... }: 
{
  networking = {
    # hostname is based on host value passed from flake.nix
    hostName = "${host}";
    networkmanager.enable = true;
    #nameservers = [ "8.8.8.8" "8.8.4.4" "1.1.1.1" ];
    firewall = {
      enable = true;
      #allowedTCPPorts = [ 22 80 443 59010 59011 ];
      #allowedUDPPorts = [ 59010 59011 ];
    };
  };

  environment.systemPackages = with pkgs; [
    networkmanagerapplet
  ];
}
