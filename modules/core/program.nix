{ pkgs, lib, ... }: 
{
  programs.dconf.enable = true;
  programs.zsh.enable = true;

  # thunar file manager
  #programs.thunar.enable = true;
  #programs.xfconf.enable = true;
  #programs.thunar.plugins = with pkgs.xfce; [
  #  thunar-archive-plugin
  #  thunar-volman
  #];

  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
    # pinentryFlavor = "";
  };
  programs.nix-ld.enable = true;
  programs.nix-ld.libraries = with pkgs; [];
}
