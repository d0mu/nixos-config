{ config, pkgs, ... }:
{
  # https://nixos.wiki/wiki/Nvidia
  
  # Load nvidia driver for Xorg and Wayland
  services.xserver.videoDrivers = ["nvidia"];

  hardware.nvidia = {
    # required by most wayland compositors!
    modesetting.enable = true;
    # default is false, but apparently needed for wayland
    powerManagement.enable = true;

    # Fine-grained power management. Turns off GPU when not in use.
    # Experimental and only works on modern Nvidia GPUs (Turing or newer).
    powerManagement.finegrained = false;

    # Use the NVidia open source kernel module (not to be confused with the
    # independent third-party "nouveau" open source driver).
    # Support is limited to the Turing and later architectures. Full list of 
    # supported GPUs is at: 
    # https://github.com/NVIDIA/open-gpu-kernel-modules#compatible-gpus 
    # Only available from driver 515.43.04+
    # Currently alpha-quality/buggy, so false is currently the recommended setting.
    open = false;

    # Enable the Nvidia settings menu,
	  # accessible via `nvidia-settings`.
    nvidiaSettings = true;

    # Optionally, you may need to select the appropriate driver version for your specific GPU. (Using .stable or .beta)
    # Note: I've hard-coded to 'latest'; see options in definition:
    #    https://github.com/NixOS/nixpkgs/blob/nixos-unstable/pkgs/os-specific/linux/nvidia-x11/default.nix
    package = config.boot.kernelPackages.nvidiaPackages.latest;

    # Laptop with hybrid graphics? --- add to the configuration.nix for the host
    # https://nixos.wiki/wiki/Nvidia#Laptop_Configuration:_Hybrid_Graphics_.28Nvidia_Optimus_PRIME.29
    #prime = {
      # Make sure to use the correct Bus ID values for your system!
      # lspci or check link to find them
    #  intelBusId = "PCI:0:2:0";
    #  nvidiaBusId = "PCI:14:0:0";
	  #};
  };
}