{ inputs, pkgs, ... }:
{
  imports = [ inputs.darkmatter-grub-theme.nixosModule ];

  boot.loader = {
    # systemd
    systemd-boot.enable = false;
    systemd-boot.configurationLimit = 10;

    # efi support
    efi.canTouchEfiVariables = true;

    # grub
    grub.enable = true;
    grub.efiSupport = true;
    grub.device = "nodev";
    grub.useOSProber = true;
    grub.default = "saved";

    # theme
    grub.darkmatter-theme = {
      enable = true;
      style = "nixos";
      icon = "color";
      resolution = "1080p";
    };
  };

}
