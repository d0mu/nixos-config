{ pkgs, ... }: 
{
  home.packages = with pkgs; [
    webcord
    vesktop 
    # if using this set configfile below to vesktop/themes instead of Vencord/themes
    #(discord.override { 
    #  withVencord = true; 
    #})
  ];
  xdg.configFile."vesktop/themes/custom.css".text = '' 
    /**
    * @name Catppuccin Mocha
    * @author winston#0001
    * @authorId 505490445468696576
    * @version 0.2.0
    * @description 🎮 Soothing pastel theme for Discord
    * @website https://github.com/catppuccin/discord
    * @invite r6Mdz5dpFc
    * **/

    @import url("https://catppuccin.github.io/discord/dist/catppuccin-mocha.theme.css");
  '';
  ## to import a file that is stored under the discord folder, next to discord.nix
  ## default.nix will need to be updated to handle the load from a folder
  #xdg.configFile."Vencord/themes/gruvbox.theme.css".source = ./gruvbox.css;
}