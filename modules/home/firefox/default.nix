{ pkgs, lib, config, username, ... }: 
{
  programs.firefox = {
    enable = true;

    profiles.${username} = {
      settings = {};
      isDefault = true;
      extensions = with pkgs.nur.repos.rycee.firefox-addons; [
        # security/privacy
        ublock-origin
        behave
        canvasblocker
        skip-redirect
        # general
        old-reddit-redirect
        reddit-enhancement-suite
        i-dont-care-about-cookies
      ];
      search = {
        force = true;
        default = "DuckDuckGo";
        order = [ "DuckDuckGo" ];
        engines = {
          "Nix Packages" = {
            urls = [{
              template = "https://search.nixos.org/packages";
              params = [
                { name = "type"; value = "packages"; }
                { name = "query"; value = "{searchTerms}"; }
              ];
            }];
            icon = "''${pkgs.nixos-icons}/share/icons/hicolor/scalable/apps/nix-snowflake.svg";
            definedAliases = [ "@np" ];
          };
        };
      };
      # arkenfox user.js
      extraConfig = builtins.readFile ./user.js;
    };
  };
}