{ inputs, pkgs, ... }: 
{
  programs.zathura = {
    enable = true;
    options = {
      ### Settings from original zathurarc ###
      pages-per-row = "1";
      scroll-page-aware = "true";
      scroll-full-overlap = "0.01";
      scroll-step = "100";
      font = "JetBrainsMono Nerd Font 10";

      # NixOS Colours Theme
      # Allow recolor
      recolor = "true";

      # Don't allow original hue when recoloring
      recolor-keephue = "true";
  
      # Don't keep original image colors while recoloring
      recolor-reverse-video = "false";

      default-fg = "#CDD6F4";
      default-bg = "#1E1E2E";

      completion-bg	= "#1E1E2E";
      completion-fg	= "#CDD6F4";
      completion-highlight-bg	= "#1E1E2E";
      completion-highlight-fg	= "#CDD6F4";
      completion-group-bg	= "#1E1E2E";
      completion-group-fg	= "#A6E3A1";

      statusbar-fg = "#CDD6F4";
      statusbar-bg = "#1E1E2E";

      notification-bg	= "#1E1E2E";
      notification-fg	= "#CDD6F4";
      notification-error-bg	= "#1E1E2E";
      notification-error-fg	= "#F38BAB";
      notification-warning-bg	= "#1E1E2E";
      notification-warning-fg	=  "#CDD6F4";

      inputbar-fg	= "#CDD6F4";
      inputbar-bg = "#1E1E2E";

      recolor-lightcolor	= "#BAC2DE";
      recolor-darkcolor	= "#45475A";

      index-fg	= "#CDD6F4";
      index-bg = "#1E1E2E";
      index-active-fg	= "#CDD6F4";
      index-active-bg	= "#1E1E2E";

      render-loading-bg	= "#1E1E2E";
      render-loading-fg	= "#CDD6F4";

      highlight-color	= "#1E1E2E";
      highlight-fg = "#CDD6F4";
      highlight-active-color = "#CDD6F4";
    };
  };
}