{ ... }:
let custom = {
    font = "JetBrainsMono Nerd Font";
    font_size = "15px";
    font_weight = "bold";
    text_color = "#cdd6f4";
    secondary_accent= "#89b4fa";
    tertiary_accent = "#f5f5f5";
    background = "#11111B";
    opacity = "0.98";
};
in 
{
  programs.waybar.style = ''

    * {
        border: none;
        border-radius: 0px;
        padding: 0;
        margin: 0;
        min-height: 0px;
        font-family: ${custom.font};
        font-weight: ${custom.font_weight};
        opacity: ${custom.opacity};
    }

    window#waybar {
        background: none;
    }

    #workspaces {
        font-size: 18px;
        padding-left: 15px;
        
    }
    #workspaces button {
        color: ${custom.text_color};
        padding-left:  6px;
        padding-right: 6px;
    }
    #workspaces button.empty {
        color: #6c7086;
    }
    #workspaces button.active {
        color: #b4befe;
    }

    #window, #tray, #pulseaudio, #network, #cpu, #memory, #disk, #clock, #battery, #custom-notification, #custom-wmname, #custom-weather, #temperature, #custom-crypto, #idle_inhibitor {
        font-size: ${custom.font_size};
        color: ${custom.text_color};
    }

    #custom-launcher {
        font-size: 20px;
        color: #b4befe;
        font-weight: ${custom.font_weight};
        padding-left: 10px;
        padding-right: 15px;
    }

    #window {
        padding-left: 25px;
    }

    #clock {
        padding-left: 9px;
        padding-right: 15px;
    }

    #cpu {
        padding-left: 15px;
        padding-right: 9px;
        margin-left: 7px;
    }
    #memory {
        padding-left: 9px;
        padding-right: 9px;
    }
    #disk {
        padding-left: 9px;
        padding-right: 9px;
    }
    #temperature {
        padding-left: 9px;
        padding-right: 9px;
    }
    #backlight {
        padding-left: 9px;
        padding-right: 15px;
    }

    #pulseaudio {
        padding-left: 15px;
        padding-right: 9px;
        margin-left: 7px;
    }
    #network {
        padding-left: 9px;
        padding-right: 9px;
    }
    #custom-crypto {
        padding-left: 9px;
        padding-right: 9px;
    }
    #idle_inhibitor {
        padding-left: 9px;
        padding-right: 15px;
    }

    #tray {
        padding-left: 15px;
        padding-right: 15px;
    }

    #battery {
        padding-left: 15px;
        padding-right: 9px;
    }

    #custom-notification {
        padding-left: 20px;
        padding-right: 20px;
    }
  '';
}
