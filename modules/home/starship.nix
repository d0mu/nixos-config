{ lib, inputs, ... }: 
{
  programs.starship = {
    enable = true;

    enableBashIntegration = true;
    enableZshIntegration = true;
    enableNushellIntegration = true;

    settings = {
      format = lib.concatStrings [
        "[](color_blue)"
        "$os"
        "$username"
        "[](bg:color_green fg:color_blue)"
        "$directory"
        "[](fg:color_green bg:color_yellow)"
        "$git_branch"
        "$git_status"
        "[](fg:color_yellow bg:color_purple)"
        "$nix_shell"
        "[](fg:color_purple bg:color_bg3)"
        "$cmd_duration"
        "[](fg:color_bg3) "
      ];

      palette = "catppuccin_mocha";

      palettes.catppuccin_mocha = {
        color_fg0 = "#1e1e2e";
        color_bg1 = "#11111b";
        color_bg3 = "#313244";
        color_blue = "#89b4fa";
        color_aqua = "#89dceb";
        color_green = "#a6e3a1";
        color_orange = "#fab387";
        color_purple = "#cba6f7";
        color_red = "#f38ba8";
        color_yellow = "#f9e2af";
      };

      palettes.gruvbox_dark = {
        color_fg0 = "#fbf1c7";
        color_bg1 = "#3c3836";
        color_bg3 = "#665c54";
        color_blue = "#458588";
        color_aqua = "#689d6a";
        color_green = "#98971a";
        color_orange = "#d65d0e";
        color_purple = "#b16286";
        color_red = "#cc241d";
        color_yellow = "#d79921";
      };


      os = {
        disabled = false;
        style = "bg:color_blue bold fg:color_fg0";
        symbols = {
          NixOS = " ";
        };
      };

      username = {
        disabled = false;
        show_always = true;
        style_user = "bg:color_blue bold fg:color_fg0";
        style_root = "bg:color_blue bold fg:color_red";
        format = "[$user ]($style)";
      };

      directory = {
        style = "bold fg:color_fg0 bg:color_green";
        format = "[ $path ]($style)";
        truncation_length = 3;
      };

      git_branch = {
        symbol = "";
        style = "bg:color_yellow";
        format = "[[ $symbol $branch ](bold fg:color_fg0 bg:color_yellow)]($style)";
      };

      git_status = {
        style = "bg:color_yellow bold fg:color_fg0";
        format = "[$all_status$ahead_behind]($style)";
      };
      
      nix_shell = {
        format = "[ via nix $name ]($style)";
        style = "bg:color_purple bold fg:color_fg0";
      };

      time = {
        disabled = false;
        time_format = "%R";
        style = "bg:color_bg1";
        format = "[[   $time ](fg:color_fg0 bg:color_bg1)]($style)";
      };

      cmd_duration = {
        format = "[ 󰔛 $duration ]($style)";
        disabled = false;
        style = "bg:color_bg3 fg:color_yellow";
        show_notifications = false;
        min_time_to_notify = 60000;
      };

      line_break = {
        disabled = false;
      };

      character = {
        disabled = false;
        success_symbol = "[  ](bold fg:color_green)";
        error_symbol = "[  ](bold fg:color_red)";
      };
    };
  };
}