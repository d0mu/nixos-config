{ hostname, config, pkgs, host, ...}: 
{
  programs.zsh = {
    enable = true;
    # -- trying without autocomplete plugin
    # zsh-autocomplete plugin requires false here:
    #enableCompletion = false;
    #zplug = {
    #  enable = true;
    #  plugins = [
    #    { name = "marlonrichert/zsh-autocomplete"; } # Simple plugin installation
    #  ];
    #};
    enableCompletion = true;
    autosuggestion.enable = true;
    syntaxHighlighting.enable = true;
    oh-my-zsh = {
      enable = true;
      plugins = [ ];
    };
    history = {
      save = 90000;
    };
    initExtraFirst = ''
      DISABLE_AUTO_UPDATE=true
      DISABLE_MAGIC_FUNCTIONS=true
      export "MICRO_TRUECOLOR=1"
    '';
    initExtra = ''
      setopt share_history 
      setopt hist_expire_dups_first
      setopt hist_ignore_dups
      setopt hist_verify
      
      # Use fd (https://github.com/sharkdp/fd) for listing path candidates.
      # - The first argument to the function ($1) is the base path to start traversal
      # - See the source code (completion.{bash,zsh}) for the details.
      _fzf_compgen_path() {
        fd --hidden --exclude .git . "$1"
      }

      # Use fd to generate the list for directory completion
      _fzf_compgen_dir() {
        fd --type=d --hidden --exclude .git . "$1"
      }

      # Advanced customization of fzf options via _fzf_comprun function
      # - The first argument to the function is the name of the command.
      # - You should make sure to pass the rest of the arguments to fzf.
      _fzf_comprun() {
        local command=$1
        shift

        case "$command" in
          cd)           fzf --preview 'eza --tree --color=always {} | head -200' "$@" ;;
          ssh)          fzf --preview 'dig {}'                   "$@" ;;
          *)            fzf --preview "$show_file_or_dir_preview" "$@" ;;
        esac
      }

      # configure `time` format
      TIMEFMT=$'\nreal\t%E\nuser\t%U\nsys\t%S\ncpu\t%P'

      # Plugin Settings
      ## zsh-auto-complete settings
      ### Limits the history/completion results shown to 1/3rd of the terminals space
      #zstyle -e ':autocomplete:*' list-lines 'reply=( $(( LINES / 3 )) )'
      ### Tab cycles the completions instead of selecting first
      #bindkey '\t' menu-complete "$terminfo[kcbt]" reverse-menu-complete
      ### NixOS Fix for zsh-autocomplete, from wiki
      #bindkey "''${key[Up]}" up-line-or-search
    
      # Functions
      ## mcd - create and cd to new directory - Usage: mcd new-folder
      mcd() {
          mkdir -p -- "$1" && cd -P -- "$1";
      }

      ## serve - Python HTTP Server - Usage: serve port
      serve() {
          PORT=$1
          DIR=$(pwd)
          echo "Serving files from $DIR --- on port $PORT"
          python3 -m http.server "$PORT"
      }

      ## hex-encode / hex-decode / rot13 - encoding+decoding functions from ParrotOS .zshrc
      hex-encode() {
        echo "$@" | xxd -p
      }
      hex-decode() {
        echo "$@" | xxd -p -r
      }

      ## rot13 cipher
      rot13() {
        echo "$@" | tr 'A-Za-z' 'N-ZA-Mn-za-m'
      }

      # Flavour
      echo ""
      echo "Welcome $USER."
      echo ""
    '';
    shellAliases = {
      # record = "wf-recorder --audio=alsa_output.pci-0000_08_00.6.analog-stereo.monitor -f $HOME/Videos/$(date +'%Y%m%d%H%M%S_1.mp4')";

      # Utils
      c = "clear";
      cd = "z";
      trash = "gtrash put";
      cat = "bat --style plain --paging never --theme ansi";
      nano = "micro";
      code = "codium";
      py = "python";
      icat = "kitten icat"; # Print an image in terminal
      dsize = "du -hs"; # Disk Usage
      pdf = "tdf";
      open = "xdg-open";
      space = "ncdu";
      man = "BAT_THEME='default' batman";

      l = "eza --icons  -a --group-directories-first -1"; #EZA_ICON_SPACING=2
      ls = "exa -1  --icons";
      ll = "eza --icons  -a --group-directories-first -1 --no-user --long";
      ld = "exa -lD --icons";
      lt = "exa -T --icons"; #tree view!
      tree = "eza --icons --tree --group-directories-first";
      # SSH terminal fix for terminal in use
      ## Alacritty
      #ssh = "TERM=xterm-256color $(which ssh)";
      ## Kitty
      ssh = "kitty +kitten ssh";

      # Nixos
      cdnix = "cd ~/nixos-config && codium ~/nixos-config";
      #ns = "nix-shell --run zsh";
      ns = "nom-shell --run zsh";
      nix-switch = "nh os switch";
      nix-update = "nh os switch --update";
      nix-clean = "nh clean all --keep 5";
      nix-search = "nh search";
      nix-test = "nh os test";

      # python
      piv = "python -m venv .venv";
      psv = "source .venv/bin/activate";
    };
  };
  programs.zoxide = {
    enable = true;
    enableZshIntegration = true;
  };
}
