{ pkgs, ... }:
{
  programs.fzf = {
    enable = true;
    
    enableZshIntegration = true;
    
    defaultCommand = "fd --hidden --strip-cwd-prefix --exclude .git";
    fileWidgetOptions = [ "--preview 'if [ -d {} ]; then eza --tree --color=always {} | head -200; else bat -n --color=always --line-range :500 {}; fi'" ];
    changeDirWidgetCommand = "fd --type=d --hidden --strip-cwd-prefix --exclude .git";
    changeDirWidgetOptions = [ "--preview 'eza --tree --color=always {} | head -200'" ];

    ## Theme
    defaultOptions = [
      "--color=fg:-1,fg+:#cdd6f4,bg:-1,bg+:#1e1e2e"
      "--color=hl:#f9e2af,hl+:#f2cdcd,info:#cba6f7,marker:#fab387"
      "--color=prompt:#f38ba8,spinner:#a6e3a1,pointer:#fab387,header:#89b4fa"
      "--color=border:#45475a,label:#a6adc8,query:#cdd6f4"
      "--border='rounded' --border-label='' --preview-window='border-rounded' --prompt='> '"
      "--marker='>' --pointer='>' --separator='─' --scrollbar='│'"
      "--info='right'"
    ];
  };
}