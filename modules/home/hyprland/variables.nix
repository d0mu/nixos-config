{ ... }:
{
  home.sessionVariables = {
    _JAVA_AWT_WM_NONEREPARENTING = "1";
    __GL_GSYNC_ALLOWED = "1";
    __GL_VRR_ALLOWED = "0";
    ANKI_WAYLAND = "1";
    CLUTTER_BACKEND = "wayland";
    DIRENV_LOG_FORMAT = "";
    DISABLE_QT5_COMPAT = "0";
    GDK_BACKEND = "wayland";
    GTK_THEME = "Dracula";
    MOZ_ENABLE_WAYLAND = "1";
    NIXOS_OZONE_WL = "1";
    QT_AUTO_SCREEN_SCALE_FACTOR = "1";
    QT_QPA_PLATFORM = "xcb";
    QT_QPA_PLATFORMTHEME = "qt5ct";
    QT_STYLE_OVERRIDE = "kvantum";
    QT_WAYLAND_DISABLE_WINDOWDECORATION = "1";
    SDL_VIDEODRIVER = "wayland";
    SSH_AUTH_SOCK = "/run/user/1000/keyring/ssh";
    WLR_BACKEND = "vulkan";
    WLR_RENDERER = "vulkan";
    WLR_DRM_NO_ATOMIC = "1";
    WLR_NO_HARDWARE_CURSORS = "1";
    XDG_SESSION_TYPE = "wayland";
    # nvidia
    GBM_BACKEND = "nvidia-drm";
    __GLX_VENDOR_LIBRARY_NAME = "nvidia";
    LIBVA_DRIVER_NAME = "nvidia";
    ELECTRON_OZONE_PLATFORM_HINT = "auto";
  };
}
