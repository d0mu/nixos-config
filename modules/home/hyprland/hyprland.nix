{ inputs, pkgs, ...}: 
{
  home.packages = with pkgs; [
    direnv
    glib
    grim
    hyprpicker
    hyprshade
    inputs.hypr-contrib.packages.${pkgs.system}.grimblast
    slurp
    swaybg
    # swww
    wayland
    wf-recorder
    wl-clip-persist
    wf-recorder
    glib
    wayland
    direnv
  ];
  systemd.user.targets.hyprland-session.Unit.Wants = [ "xdg-desktop-autostart.target" ];
  wayland.windowManager.hyprland = {
    enable = true;
    xwayland = {
      enable = true;
      # hidpi = true;
    };
    systemd.enable = true;
  };
}
