{ inputs, pkgs, ... }: 
{
  home.packages = (with pkgs; [
    # General
    bitwise                           # cli tool for bit / hex manipulation
    cbonsai                           # bonsai screensaver
    chromium                          # plain chromium backup browser
    cmatrix                           # simulates the matrix
    copyq                             # wayland clipboard manager/history
    dropbox                           # cloud storage service
    entr                              # perform action when files change
    eza                               # ls replacement
    fd                                # find replacement
    file                              # show file information 
    fzf                               # fuzzy finder
    gifsicle                          # gif utility
    gimp                              # image editor
    gtt                               # google translate TUI
    gtrash                            # rm replacement, put deleted files in system trash
    hexdump                           # hex dump utility
    keepassxc                         # password manager
    lazygit                           # git terminal UI
    libreoffice                       # office suite
    meld                              # diff with a gui
    nautilus                          # file manager
    neofetch                          # system fetch utility
    nitch                             # another system fetch utility
    nix-prefetch-github               # Nix package manager utility
    pipes                             # terminal screensaver
    obsidian                          # markdown editor and vault
    ripgrep                           # grep replacement
    soundwireserver                   # pass audio to android phone
    #thunderbird-unwrapped             # thunderbird client
    #tldr                              # tl;dr of man pages
    tealdeer                          # Rust version of tldr, way faster
    todo                              # cli todo list
    toipe                             # typing test in the terminal
    winetricks                        # utility for managing Wine dependencies
    wineWowPackages.wayland           # Wine packages for Wayland
    yazi                              # terminal file manager
    yt-dlp-light                      # youtube-dl fork based on the now inactive youtube-dlc

    # Utils / Required
    android-tools                     # adb
    appimage-run                      # for AppImages
    ark                               # archive tool
    bleachbit                         # cache cleaner
    ffmpeg                            # multimedia framework
    file                              # Show file information
    freerdp                           # remote desktop protocol client
    gparted                           # partition manager
    imv                               # image viewer
    killall                           # kill but for all given processes 
    libnotify                         # notification library
    man-pages                         # extra man pages
    mpv                               # video player
    ncdu                              # disk space
    openssl                           # cryptography library and toolkit
    pamixer                           # pulseaudio command line mixer
    pavucontrol                       # pulseaudio volume control (GUI)
    playerctl                         # controller for media players
    poweralertd                       # power outage alerting daemon
    qalculate-gtk                     # calculator
    remmina                           # remote desktop client
    unzip                             # decompression tool for zip files
    wget                              # retrieves files from the web
    wl-clipboard                      # clipboard utils for wayland (wl-copy, wl-paste)
    xdg-utils                         # desktop integration utilities for the X Window System
    xxd                               # hex dump utility
    zenity                            # display dialogs from the commandline/scripts
    inputs.alejandra.defaultPackage.${system}

    # Pentesting
    aircrack-ng                       # Wi-Fi network security testing tool
    bandwhich                         # terminal bandwidth utilisation tool
    ffuf                              # web fuzzer
    hashcat                           # password cracker
    john                              # password cracker
    nmap                              # network exploration tool
    rustscan                          # fast port scanner written in Rust
    socat                             # multipurpose relay (SOcket CAT)

    # Development
    # Java
    #jdk17

    # C / C++
    # gcc
    # gnumake
    # valgrind                          # c memory analyzer

    # Python
    (python3.withPackages(ps: with ps; [ 
      requests
    ]))
    pipx                              # isolated python apps
  ]);
}
